package bank;

import java.io.Serializable;

public class SpendingAccount extends Account implements Serializable {

	

	/**
	 * 
	 */
	private static final long serialVersionUID = 6320554641433819963L;


	public SpendingAccount(String accountID , double amount)  {
		super(accountID, amount);
	}


	public void addMoney(double sum)  {
	    setChanged();
	    notifyObservers("In contul :"+this.accountID+" s-a adaugat suma de : "+sum+" RON");
		this.amount=this.amount+sum;
		
	}
	
	@Override
	public String toString() {
		return " ACCOUNT ID: "+this.accountID+"\n TYPE ACCOUNT: Spending Account "+" \n TOTAL AMOUNT :"+this.amount+" RON\n\n ";
	}


	public void withdraw(double sum) {

		if(this.amount-sum>0){
			 setChanged();
			 notifyObservers("Din contul :"+this.accountID+" s-a extras suma de : "+sum+" RON");
			this.amount= this.amount-sum;}
			
		
	}

}
