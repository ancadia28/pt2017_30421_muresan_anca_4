package bank;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;

public class GUI {

	private Bank b;

	public GUI() {
		b = new Bank();
		try {
			b = BackUp.deserialize();
		} catch (FileNotFoundException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		} catch (ClassNotFoundException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		} catch (IOException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
	}

	public static void errorEncountered(String message) {
		JFrame frame1 = new JFrame("ERROR ");
		frame1.setSize(500, 300);
		frame1.setBackground(Color.CYAN);
		frame1.setLocation(300, 300);
		JPanel panel1 = new JPanel();
		panel1.setLayout(null);
		panel1.setBounds(500, 0, 500, 300);
		panel1.setBackground(Color.lightGray);
		frame1.add(panel1);

		JLabel l1 = new JLabel(message);
		l1.setBounds(30, 70, 500, 50);
		panel1.add(l1);

		frame1.setVisible(true);
	}

	public void doGUI() {

		JFrame frame1 = new JFrame("BRD ");
		frame1.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame1.setSize(400, 400);
		frame1.setBackground(Color.CYAN);

		JPanel panel1 = new JPanel();
		panel1.setLayout(null);
		panel1.setBounds(400, 0, 400, 400);
		panel1.setBackground(Color.lightGray);
		frame1.add(panel1);

		JButton addClient = new JButton("ADD CLIENT");
		JButton addAccount = new JButton("ADD ACCOUNT");
		JButton addMoney = new JButton("ADD/WITHDRAW MONEY");
		JButton viewAccountDetails = new JButton("MANAGE ACCOUNTS");

		addClient.setBounds(100, 100, 200, 30);
		addAccount.setBounds(100, 150, 200, 30);
		addMoney.setBounds(100, 200, 200, 30);
		viewAccountDetails.setBounds(100, 250, 200, 30);

		panel1.add(addClient);
		panel1.add(addAccount);
		panel1.add(addMoney);
		panel1.add(viewAccountDetails);

		addClient.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {

				JFrame frame1 = new JFrame("ADD CLIENT ");
				frame1.setSize(500, 500);
				frame1.setBackground(Color.CYAN);

				JPanel panel1 = new JPanel();
				panel1.setLayout(null);
				panel1.setBounds(500, 0, 500, 500);
				panel1.setBackground(Color.lightGray);
				frame1.add(panel1);

				JButton b1 = new JButton("ADD");

				JLabel l1 = new JLabel("SSN ");
				JLabel l2 = new JLabel("Name ");
				JLabel l3 = new JLabel("Birth: ");

				JTextField t1 = new JTextField();
				JTextField t2 = new JTextField();
				JTextField t3 = new JTextField();

				t1.setBounds(200, 100, 100, 30);
				t2.setBounds(200, 200, 100, 30);
				t3.setBounds(200, 300, 100, 30);
				l1.setBounds(100, 100, 100, 30);
				l2.setBounds(100, 200, 100, 30);
				l3.setBounds(100, 300, 100, 30);
				b1.setBounds(150, 370, 100, 30);

				panel1.add(t1);
				panel1.add(t2);
				panel1.add(t3);
				panel1.add(l1);
				panel1.add(l2);
				panel1.add(l3);
				panel1.add(b1);

				b1.addActionListener(new ActionListener() {

					public void actionPerformed(ActionEvent e) {

						String name = null;
						String date = null;
						if (t2.getText().isEmpty())
							name = "";
						else
							name = t2.getText();
						if (t3.getText().isEmpty())
							date = "";
						else
							date = t3.getText();
						Person newP = new Person(t1.getText(), name, date);
						b.addPerson(newP);
						try {
							BackUp.serialize(b);
						} catch (IOException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						System.out.println(newP.getName());

					}

				});
				frame1.setVisible(true);
			
			}

		});

		addAccount.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				JFrame frame1 = new JFrame("ADD ACCOUNT ");
				frame1.setSize(500, 500);
				frame1.setBackground(Color.CYAN);

				JPanel panel1 = new JPanel();
				panel1.setLayout(null);
				panel1.setBounds(500, 0, 500, 500);
				panel1.setBackground(Color.lightGray);
				frame1.add(panel1);

				JButton b1 = new JButton("SUBMIT");
				JLabel l1 = new JLabel("SSN ");
				JLabel l2 = new JLabel("Account ID ");
				JLabel l3 = new JLabel("Amount");
				JLabel l4 = new JLabel("WhenCreated");
				JLabel l5 = new JLabel("DepositPeriod");

				JTextField t1 = new JTextField();
				JTextField t2 = new JTextField();
				JTextField t3 = new JTextField();
				JTextField t4 = new JTextField();
				JTextField t5 = new JTextField();

				t1.setBounds(200, 100, 100, 30);
				t2.setBounds(200, 150, 100, 30);
				t3.setBounds(200, 200, 100, 30);
				t4.setBounds(200, 250, 100, 30);
				t5.setBounds(200, 300, 100, 30);

				l1.setBounds(100, 100, 100, 30);
				l2.setBounds(100, 150, 100, 30);
				l3.setBounds(100, 200, 100, 30);
				l4.setBounds(100, 250, 100, 30);
				l5.setBounds(100, 300, 100, 30);

				b1.setBounds(150, 370, 100, 30);

				panel1.add(t1);
				panel1.add(t2);
				panel1.add(t3);
				panel1.add(t4);
				panel1.add(t5);
				panel1.add(l5);
				panel1.add(l1);
				panel1.add(l2);
				panel1.add(l3);
				panel1.add(l4);
				panel1.add(b1);

				b1.addActionListener(new ActionListener() {

					public void actionPerformed(ActionEvent e) {

						if (b.searchPerson(t1.getText()) != null) {
							Account newAccount = null;
							if (t2.getText().startsWith("Sa")) {

								newAccount = new SavingAccount(t2.getText(), Double.parseDouble(t3.getText()),
										Integer.parseInt(t5.getText()));
							} else {
								newAccount = new SpendingAccount(t2.getText(), Double.parseDouble(t3.getText()));

							}

							newAccount.setAccountId(t2.getText());

							b.addAccount(b.searchPerson(t1.getText()), newAccount);
							try {
								BackUp.serialize(b);
							} catch (IOException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
						} else {
							errorEncountered("Could not find the person ! Enter a valid SSN");
						}

					}
				});
				frame1.setVisible(true);

			}

		});

		addMoney.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {

				JFrame frame2 = new JFrame("TRANSACTIONS");
				frame2.setSize(600, 500);
				frame2.setBackground(Color.CYAN);
				JPanel p = new JPanel();
				p.setLayout(null);
				p.setBounds(0, 0, 600, 500);
				frame2.add(p);
				JComboBox<String> box1 = new JComboBox<String>();
				JComboBox<String> box2 = new JComboBox<String>();
				JComboBox<String> box3 = new JComboBox<String>();

				box1.addItem(" ");
				box2.addItem(" ");

				JLabel l1 = new JLabel("Choose SSN");
				JLabel l2 = new JLabel("Choose Account");
				JLabel l3 = new JLabel("Choose Operation");
				JLabel l4 = new JLabel("Available money:");
				JLabel l5 = new JLabel("Enter sum");
				JTextField t1 = new JTextField();
				JButton b1 = new JButton("SUBMIT");

				l1.setBounds(150, 50, 189, 41);
				l2.setBounds(150, 100, 189, 41);
				l3.setBounds(145, 150, 189, 41);
				l4.setBounds(100, 200, 189, 41);
				l5.setBounds(100, 250, 189, 41);
				t1.setBounds(180, 250, 100, 41);
				b1.setBounds(250, 350, 100, 41);

				box1.setBounds(250, 50, 189, 41);
				box2.setBounds(250, 100, 189, 41);
				box3.setBounds(250, 150, 189, 41);

				for (int i = 0; i < b.getPersonList().size(); i++) {

					box1.addItem(b.getPersonList().get(i).getSSN());

				}

				box1.addActionListener(new ActionListener() {

					public void actionPerformed(ActionEvent e) {
						box2.removeAllItems();
						String selected = (String) box1.getSelectedItem();
						if (b.searchPerson(selected) != null && b.getAccountList(b.searchPerson(selected)) != null) {
							for (int j = 0; j < b.getAccountList(b.searchPerson(selected)).size(); j++) {
								box2.addItem(b.getAccountList(b.searchPerson(selected)).get(j).getAccountId());
							}

						}
					}
				});

				box3.addItem("");
				box3.addItem("Add");
				box3.addItem("Withdraw");

				box2.addActionListener(new ActionListener() {

					public void actionPerformed(ActionEvent e) {
						Person x = b.searchPerson((String) box1.getSelectedItem());
						List<Account> accounts = b.getAccountList(x);
						if (accounts != null) {
							for (int i = 0; i < accounts.size(); i++)
								if (accounts.get(i).getAccountId().equals((String) box2.getSelectedItem())) {
									l4.setText("Available money: " + accounts.get(i).getAmount());

								}
						}
					}
				});

				b1.addActionListener(new ActionListener() {

					public void actionPerformed(ActionEvent e) {

						Person x = b.searchPerson((String) box1.getSelectedItem());
						List<Account> accounts = b.getAccountList(x);

						if (((String) box3.getSelectedItem()).equals("Add")) {
							if (accounts != null) {
								for (int i = 0; i < accounts.size(); i++)
									if (accounts.get(i).getAccountId().equals((String) box2.getSelectedItem())) {

										if (t1.getText().isEmpty() == false) {
											b.depositMoney(x, (String) box2.getSelectedItem(), Double.parseDouble(t1.getText()));
										}
										l4.setText("Available money: " + accounts.get(i).getAmount());

										if (((String) box2.getSelectedItem()).contains("Sa")) {
											errorEncountered(
													"Your are not allowed to add money on your saving account");
										}

									}
							}

						} else {

							if (accounts != null) {
								for (int i = 0; i < accounts.size(); i++)
									if (accounts.get(i).getAccountId().equals((String) box2.getSelectedItem())) {
										if (t1.getText().isEmpty() == false)
											//accounts.get(i).withdraw((Double.parseDouble(t1.getText())));
											b.withdrawMoney(x, (String) box2.getSelectedItem(), Double.parseDouble(t1.getText()));

										l4.setText("Available money: " + accounts.get(i).getAmount());

										if (((String) box2.getSelectedItem()).contains("Sa")) {
											errorEncountered(
													"You are not allowed to get your money from your spanding account yet!");
										}

									}
							}
						}

						try {
							BackUp.serialize(b);
						} catch (IOException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
					}
				});
				p.add(box1);
				p.add(box2);
				p.add(box3);
				p.add(l1);
				p.add(l2);
				p.add(l3);
				p.add(l4);
				p.add(l5);
				p.add(t1);
				p.add(b1);

				frame2.setVisible(true);

			}

		});

		viewAccountDetails.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				JFrame frame1 = new JFrame("ACOUNT DETAILS ");
				frame1.setSize(800, 800);
				frame1.setBackground(Color.CYAN);

				JPanel panel1 = new JPanel();
				panel1.setLayout(null);
				panel1.setBounds(800, 0, 800, 800);
				panel1.setBackground(Color.lightGray);

				JButton b1 = new JButton("DELETE CLIENT");
				JButton b2 = new JButton("DELETE ACCOUNT");

				b1.setBounds(500, 550, 150, 30);
				b2.setBounds(500, 600, 150, 30);

				frame1.add(b2);
				frame1.add(b1);
				frame1.add(panel1);

				String SSN, name, bornDate, accountId, amount, dateWhenCreated = null, type = "";
				List<Account> accounts;
				String[] cols = { "SSN", "name", "Borndate", "accountId", "type ", "amount", "dateWhenCreated" };
				ArrayList<String[]> list = new ArrayList<String[]>();
				for (int i = 0; i < b.getPersonList().size(); i++) {
					accounts = b.getAccountList(b.getPersonList().get(i));
					SSN = b.getPersonList().get(i).getSSN();
					name = b.getPersonList().get(i).getName();
					bornDate = b.getPersonList().get(i).getBornDate();
					if (accounts != null) {
						for (int j = 0; j < accounts.size(); j++) {
							accountId = accounts.get(j).getAccountId();
							if (accountId != null) {
								if (accountId.startsWith("Sa"))
									type = "Saving";
								else
									type = "Spending";
							}
							amount = String.valueOf(accounts.get(j).getAmount());
							dateWhenCreated = String.valueOf(accounts.get(j).getDate());
							list.add(new String[] { SSN, name, bornDate, accountId, type, amount, dateWhenCreated });
						}
					} else {
						list.add(new String[] { SSN, name, bornDate, " ", " ", " ", " " });
					}
				}

				String[][] elts = new String[list.size()][];
				elts = list.toArray(elts);
				JTable table = new JTable(elts, cols);
				JScrollPane pane = new JScrollPane(table, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
						JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
				table.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
				table.setBounds(300, 300, 300, 300);
				frame1.getContentPane().add(pane);

				b1.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						int row = table.getSelectedRow();
						int column = table.getSelectedColumn();
						String value = table.getModel().getValueAt(row, column).toString();
						b.removePerson(value);
						try {
							BackUp.serialize(b);
						} catch (IOException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}

					}

				});

				b2.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						int row = table.getSelectedRow();
						int column = table.getSelectedColumn();
						String value = table.getModel().getValueAt(row, column).toString();
						Person p = b.searchPerson(table.getModel().getValueAt(row, 0).toString());
						List<Account> account = b.getAccountList(p);
						for (int i = 0; i < account.size(); i++) {
							if (account.get(i).getAccountId().equals(value)) {
								account.remove(i);
							}
						}
						try {
							BackUp.serialize(b);
						} catch (IOException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}

					}

				});
				frame1.setVisible(true);

			}
		});

		frame1.setVisible(true);
	}
}