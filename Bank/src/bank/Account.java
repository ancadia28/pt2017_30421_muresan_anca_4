package bank;

import java.io.Serializable;
import java.util.Date;
import java.util.Observable;

public abstract class Account extends Observable implements Serializable{
 
/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
protected String accountID;
  protected double amount;
  protected Date whenCreated;
  
  public Account(String accountID  , double amount){
//	  if(amount < Constants.minimAmount )
//			throw new accountException("The amount  must be bigger than : "+Constants.minimAmount);
//	  if(amount > Constants.maximAmount )
//			throw new accountException("The amount  must be less than : "+Constants.maximAmount);
	 
	  this.accountID=accountID;
	  this.amount=amount;
	  this.whenCreated= new Date();
  }
  
  public String getAccountId(){
	  return this.accountID;
  }
  public void setAmount(double amount) {
		this.amount=amount;
		setChanged();
	    notifyObservers("The account "+this.accountID+"was created");
		
	}
  
  public double getAmount (){
	  return this.amount;
  }
  
 
  public Date getDate(){
	  return this.whenCreated;
  }
  
   public void setAccountId(String accountId){
	   this.accountID=accountId;
   }
   public abstract String toString();
   public abstract void withdraw (double sum);
   public abstract void addMoney(double sum) ;
   
   public boolean isWellFormed(){
	   if(this.accountID==" " || this.amount<0 || this.accountID.startsWith("S")==false || amount < Constants.minimAmount || amount > Constants.maximAmount )
		   return false;
	   if ( accountID.startsWith("Sa") == false &&  accountID.startsWith("Sp") == false )
		   return false;
	   return true;
   }
}

