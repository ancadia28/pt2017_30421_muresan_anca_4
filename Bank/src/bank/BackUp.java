package bank;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;


public class BackUp  {

	
	
	public  static void serialize(Bank bank) throws IOException {
		FileOutputStream fOut = new FileOutputStream("bank.ser"); // ia un fisier in care sa scrie
		ObjectOutputStream out = new ObjectOutputStream(fOut); // scrii un obiect intreg
		out.writeObject(bank);
		out.flush();
		out.close();
		fOut.close();
	}

	public static  Bank deserialize() throws FileNotFoundException, IOException, ClassNotFoundException {
		FileInputStream fIn = new FileInputStream("bank.ser");
		ObjectInputStream in = new ObjectInputStream(fIn);
		Bank bank = (Bank) in.readObject();
		in.close();
		fIn.close();
		return bank;

	}
}

