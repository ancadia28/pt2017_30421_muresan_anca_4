package bank;

import java.io.Serializable;
import java.util.Observable;
import java.util.Observer;

public class Person implements Observer,Serializable{
 
	private static final long serialVersionUID = 1L;
	private String SSN;
	private String name;
	private String bornDate;
	
	public Person( String SSN , String name , String date) {
//		if(SSN.matches(("\\d{3}-\\d{2}-\\d{4}")) == false){
//			throw new accountException("Please enter a valid SSN ");
//		}
			
			this.SSN=SSN;
			this.name=name;
			this.bornDate=date;
		}
	
	 @Override
	    public int hashCode() {
	        int result = 17;
	        result = 31 * result + name.hashCode();
	        result = 31 * result + bornDate.hashCode();
	        result = 31 * result + SSN.hashCode();
	        return result;
	    }
	 @Override
	    public boolean equals(Object o) {

	        if (o == this) return true;
	        if (!(o instanceof Person)) {
	            return false;
	        }

	        Person user = (Person) o;

	        return user.name.equals(name) &&
	                user.bornDate == bornDate &&
	                user.SSN.equals(SSN);
	    }


		
	public void setSSN(String SSN){
		this.SSN=SSN;
	}
	
	public String getSSN(){
		return this.SSN;
	}
	
	public void setBornDate(String bornDate){
		this.bornDate=bornDate;
	}
	
	public String getBornDate(){
		return this.bornDate;
		
	}
	
	public String getName(){
		return this.name;
	}
	
	public void setName(String name){
		this.name=name;
	}
	
	public String toString(){
		return "Holder name : "+this.name+"\n SSN:"+this.SSN+"\n\n";
	}

	@Override
	public void update(Observable o, Object arg) {
		System.out.println(arg.toString());
		
	}
	
	public boolean isWellFormed(){
		if(this.name == "" || this.SSN.matches(("\\d{3}-\\d{2}-\\d{4}")) == false || this.bornDate =="")
                 return false;
                 return true;
	}
	
	}
	

