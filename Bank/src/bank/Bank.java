package bank;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.swing.JTable;

public class Bank implements BankProc,Serializable {
   
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private HashMap<Person , List<Account> > hashMap ; 
	 private List<Person> personList;
	 
	 public Bank(){
		 hashMap = new HashMap<Person,List<Account>>();
		 personList = new ArrayList<Person>();
	 }

    
    
    public void  addAccount(Person p , Account a){
    	assert p!=null;
    	assert this.isWellFormed() : "The bank is not well formed";
    	assert p.isWellFormed() : "The person is not well formed";
    	assert a.isWellFormed() : "The account is not well formed";
    	List<Account> list = hashMap.get(p); 
    	if (list == null){
    		list = new ArrayList<Account>();
    		list.add(a);
    		hashMap.put(p, list);	
    	}
    	else{
    		list.add(a);
    	}
    	a.addObserver(p);
    	
    	assert this.isWellFormed() : "The bank is not well formed";
    }
    
    public List<Person> getPersonList(){
    	return this.personList;
    }
    
    public List<Account> getAccountList(Person p){
    	return this.hashMap.get(p);
    }
    
    public void removeAccount (Person p, String accountId) {
    	int size;
    	assert p!=null ;
    	assert personList.contains(p) :"Persoana  "+p.getSSN()+"nu e in lista ";
    	assert hashMap.get(p) != null :"Persoana "+p.getSSN()+" nu este in hashMap";
    	List<Account> list = hashMap.get(p); 	
    	size=list.size();
    		for (int i=0;i<list.size();i++){
    			if(list.get(i).getAccountId() == accountId){
    				
    				list.remove(i);			
    		}	
    	}
    		assert list.size()==size-1;

    }
    
    
   


	@Override
	public void depositMoney(Person p, String account_id, double sum)   {
		if(account_id.startsWith("Sp")){
		assert sum>Constants.minimAmount :"The sum must be biggert than"+Constants.minimAmount;
		assert sum<Constants.maximAmount :"The sum must be smaller than"+Constants.maximAmount;}
		assert hashMap.containsKey(p): "The person with "+p.getSSN()+" does not exist in the bank" ;
		assert hashMap.get(p)!=null : "The person with "+p.getSSN()+" has no accounts available";
		List<Account> list = hashMap.get(p); 
    		for(int i=0;i<list.size();i++){
    			if(list.get(i).getAccountId()==account_id)
    				list.get(i).addMoney(sum);
    				
    		}
	}



	@Override
	public void withdrawMoney(Person p, String account_id, double sum)  {
		if(account_id.startsWith("Sp")){
		assert sum>Constants.minimAmount :"The sum must be biggert than"+Constants.minimAmount;
		assert sum<Constants.maximAmount :"The sum must be smaller than"+Constants.maximAmount;}
		assert hashMap.containsKey(p): "The person with "+p.getSSN()+" does not exist in the bank" ;
		assert hashMap.get(p)!=null : "The person with "+p.getSSN()+" has no accounts available";
		List<Account> list = hashMap.get(p); 
    		for(int i=0;i<list.size();i++){
    			if(list.get(i).getAccountId()==account_id)
    				{  if(account_id.startsWith("Sp"))
    				assert list.get(i).getAmount()-sum >=0 :" The sum is too big , please enter a smaller one !";
    				list.get(i).withdraw(sum);
    				 
    				}
    	}
	}

	@Override
	public void addPerson(Person p) {
		assert p != null : "The person must not be null"; // pre 
		assert p.isWellFormed(): "The person is not well formed";
		int size = personList.size();
		if(personList.contains(p)== false)
			personList.add(p);
		assert personList.size() == size + 1;
	}



	@Override
	public void removePerson(String SSN) {
		for(int i=0;i<personList.size();i++)
			if(personList.get(i).getSSN().contains( SSN)){
				hashMap.remove(personList.get(i));
				personList.remove(i);
			}
		
	}



	@Override
	public JTable statistics(Person p) {
		String[] cols = { "name","type", "account", "money" };
		ArrayList<String[]> list = new ArrayList<String[]>();

		if(hashMap.containsKey(p)){
			List<Account> acc = hashMap.get(p); 
	    for(int i=0;i<acc.size();i++)
	    {
				String account_id = acc.get(i).accountID;
				String type;
				if(account_id.startsWith("Sa")){
					type="Saving account";
				}
				else{
					type="Spending account";
				}
				java.util.Date whenCreated = acc.get(i).whenCreated;
				double amount = acc.get(i).amount;
				list.add(new String[] {account_id,type,String.valueOf(whenCreated), String.valueOf(amount) });
		
		}
		String[][] elts = new String[list.size()][];
		elts = list.toArray(elts);
		JTable table = new JTable(elts, cols);
		table.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		return table;
		
	}	
		return null;
    }

	@Override
	public Person searchPerson(String SSN) {
		for(int i=0;i<personList.size();i++)
		
			if(personList.get(i).getSSN().contains(SSN)){
				return personList.get(i);
			}
		
		return null;
	}

public boolean isWellFormed(){
	for(int i=0;i<this.personList.size();i++){
		if(this.personList.get(i).isWellFormed()== false)
			return false;
		else{
		List <Account> list =this.hashMap.get(this.personList.get(i));
		if(list!=null){
		for(int j=0;j<list.size();j++){
			if(list.get(i).isWellFormed()==false)
				return false;
			
		}
		}
		
		}
	}
	return true;
}

}

   

