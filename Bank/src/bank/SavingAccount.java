package bank;

import java.io.Serializable;

public class SavingAccount extends Account implements Serializable{

	private static final long serialVersionUID = 1L;
	private int depositPeriod ; // number of months
	
	public SavingAccount(String accountID, double amount, int depositPeriod)  {
		super(accountID, amount);
		this.depositPeriod=depositPeriod;
	}
	
	public double calculateInterest(){
		return (amount/100)*Constants.interestPercentage*this.depositPeriod;
	}
	
	
	@Override
	public String toString() {
		return " ACCOUNT ID: "+this.accountID+"\n TYPE: Saving account \n TOTAL AMOUNT :"+this.amount+" RON\n DEPOSIT PERIOD: "+this.depositPeriod+"\n INTEREST PERCENTAGE: "+Constants.interestPercentage+"\n\n";
	}

	@Override
	public void withdraw(double sum) {
		
		setChanged();
	    notifyObservers("You cannot withdraw money from your saving account "+this.accountID);
		}
		
	

	@Override
	public void addMoney(double sum) {
		setChanged();
	    notifyObservers("You are not allowed to add money on your saving account with the id !"+this.accountID);
	}


}
