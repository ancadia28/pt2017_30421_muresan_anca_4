package bank;

import java.util.List;

import javax.swing.JTable;

public interface BankProc {
    /*
     * @ pre p!= NULL
     * @ post personList.size() = personList@pre.size +1 ;
     */
	public void addPerson(Person p);
	
	  /*@pre personList.size() != 0
	   * SSN.matches(("\\d{3}-\\d{2}-\\d{4}")) == false
	   * @post personList.size() = personList@pre.size -1 ;
	   */
	public void removePerson(String SSN);
	/*
	 * @pre p!=null
	 * @post getAccountList(p).size() =  getAccountList(p)@pre.size +1 ;
	 */
	public void addAccount(Person p , Account a);
	/*
	 * @pre p!=null
	 * @post getAccountList(p).size() =  getAccountList(p)@pre.size -1 ;
	 */
	public void removeAccount(Person p ,String accountID);
	
	/*
	 * @pre sum > Constants.minimuAmount;
	 * @pre sum < Constants.maximuAmount;
	 * @preassert hashMap.get(p)!=null;
	 * @ppreassert hasMap.contains(p);
	 * @post account.amount = pre account.ammount +sum;
	 */
	public void depositMoney(Person p,String account_id ,double sum) ;
	/*
	 * @pre sum > Constants.minimuAmount;
	 * @pre sum < Constants.maximuAmount;
	 * @preassert hashMap.get(p)!=null;
	 * @ppreassert hasMap.contains(p);
	 * @pre account.amount - sum >=0
	 * @post account.amount = pre account.ammount -sum;
	 * 
	 */
	public void withdrawMoney(Person p,String account_id ,double sum) ;
	public JTable statistics(Person p);
	public Person searchPerson(String SSN);
	public List<Person> getPersonList();
	public List<Account> getAccountList(Person p);
	
}
